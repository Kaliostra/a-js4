const filmsURL = "https://ajax.test-danit.com/api/swapi/films";
const olList = document.getElementById("film");

const getResponse = (starWars) =>
  fetch(starWars).then((response) => response.json());

getResponse(filmsURL)
  .then((films) => {
    return filmName(films);
  })
  .then((artists) => {
    artsName(artists);
  });

function filmName(films) {
  const artists = [];
  films.forEach((element) => {
    olList.innerHTML += `<li>
   Episode ${element.episodeId} : ${element.name}
   <p> Description: ${element.openingCrawl} <p/>
       Characters: 
   </li>`;
    artists.push(element.characters);
  });
  return artists;
}

function artsName(artists) {
  const filmLists = document.getElementsByTagName("li");
  artists.forEach(function (artists, i) {
    const filmList = filmLists[i];
    artists.forEach((person) => {
      getResponse(person).then(({ name }) => {
        filmList.innerHTML += `<span> <strong> ${name},<strong/> <span/>`;
      });
    });
  });
}
